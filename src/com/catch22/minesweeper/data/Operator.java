package com.catch22.minesweeper.data;

import com.catch22.minesweeper.Minesweeper;

//Class to perform operations on the grid, such as flood fill and checking for a win
public abstract class Operator
{
	public boolean[][] discoveredSquare; //boolean array to store if the corresponding grid value has been discovered

	private static int height = Minesweeper.height, width = Minesweeper.width, mines = Minesweeper.mines;

	private int max(int a, int b)
	{
		return a>b?a:b;
	}

	private int min(int a, int b)
	{
		return a<b?a:b;
	}

	public void floodFill(int[][] grid, int seedRow, int seedCol) //recursive function to clear all adjacent squares if the user has clicked on a 0
	{
		/*
		 * Takes a 3x3 (or smaller) box around the seed coordinates being passed
		 * and iterates over them row wise
		 * Boxes are smaller than 3x3 when in vicinity of an edge
		 * max and min functions are used to crop the 3x3 box such that it never goes outside the array
		 */
		for(int row = max(0,  seedRow-1); row<=min(grid.length-1, seedRow+1); row++)
			for(int col=max(0, seedCol-1); col<=min(grid[row].length-1, seedCol+1); col++)
				if(!discoveredSquare[row][col])
				{
					discoveredSquare[row][col] = true;
					if(grid[row][col] == 0)
					{
						floodFill(grid, row, col);
					}
				}
	}

	public boolean won() //returns true if you've clicked on everything apart from all the mines
	{
		int undiscoveredCount = 0;
		for(int rowIndex = 0; rowIndex < height; rowIndex++)
			for(int colIndex = 0; colIndex < width; colIndex++)
				if(!discoveredSquare[rowIndex][colIndex])
					undiscoveredCount ++;

		return undiscoveredCount == mines ? true : false;
	}

	public Operator()
	{
		width = Grid.width;
		height = Grid.height;
		mines = Grid.mines;

		discoveredSquare = new boolean[height][width];
	}

	protected boolean linearSearch(int[]a, int value)
	{
		for(int i=0; i < mines; i++)
			if(a[i] == value)
				return true;
		return false;
	}
}