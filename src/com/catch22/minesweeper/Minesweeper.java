package com.catch22.minesweeper;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import com.catch22.minesweeper.gui.*;
import com.catch22.minesweeper.data.Grid;

public class Minesweeper
{
	private Grid grid; //active grid
	private Console game; //active window

	public static int width = 9, height = 9, mines = 10;

	private static final Color revealedButtonColor = new Color(238, 238, 238), winBackGroundColor = Color.green, loseBackGroundColor = Color.red;

	public Minesweeper() //constructor
	{
		game = new Console(); //initialize window
		grid = new Grid(); //initialize grid

		game.newGame.addActionListener(new NewGame()); //make the New Game pull down menu item clickable

		for(int rowIndex = 0; rowIndex < Grid.height; rowIndex ++)
		{
			for(int colIndex = 0; colIndex < Grid.width; colIndex ++)
			{
				game.buttons[rowIndex][colIndex].addActionListener(new LeftClick(rowIndex, colIndex)); //add a left click listener for each button
				game.buttons[rowIndex][colIndex].addMouseListener(new RightClick(rowIndex, colIndex)); //add a right click listener for each button
			}
		}

		game.customize.addActionListener(new CustomizeConsole(game));//make the Customize pull down menu item clickable
	}

	public static void main(String[] args)
	{
		new Minesweeper(); //initialize game
	}

	private class NewGame implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			for(int rowIndex = 0; rowIndex < Grid.height; rowIndex ++)
			{
				for(int colIndex = 0; colIndex < Grid.width; colIndex ++)
				{
					game.buttons[rowIndex][colIndex].setIcon(null); //reset all icons of each button
					game.buttons[rowIndex][colIndex].setBackground(Console.buttonColor); //reset the color of each button
				}
			}

			game.gamePanel.setBackground(null); //set background of the game panel to null (from red if lose, from green if win)
			grid = new Grid(); //initialize a new grid to play with
		}
	}

	private class CustomizeConsole implements ActionListener
	{
		private Console mainWindow;

		public CustomizeConsole(Console mainWindow)
		{
			this.mainWindow = mainWindow;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			new Customize(mainWindow); //initialize a customize window
		}
	}

	private class LeftClick implements ActionListener
	{
		private int rowIndex, colIndex;

		public LeftClick(int rowIndex, int colIndex) //set the row index and column index to know which button we are talking about
		{
			this.rowIndex = rowIndex;
			this.colIndex = colIndex;
		}

		//method to change the look of all buttons after each move
		private void update()
		{
			for(int row = 0; row < height; row ++)
				for(int col = 0; col < width; col ++)
					if(grid.discoveredSquare[row][col]) //current square has been discovered, so change how it looks
						changeClickedButtonLook(game.buttons[row][col], grid.grid[row][col]);
		}

		private void changeClickedButtonLook(JButton b, int value) //method to change the look of a single button
		{
			b.setBackground(revealedButtonColor); //set the background color to the color of a revealed button
//			b.setEnabled(false); //you can't click on something that's already been clicked
			if(value != 0) //non zero value
				b.setIcon(new ImageIcon("res/icons/" + String.valueOf(value) + ".png")); //set the icon of the button
		}

		@Override
		public void actionPerformed(ActionEvent event)
		{
			grid.discoveredSquare[rowIndex][colIndex] = true; //mark the clicked on square as discovered

			if(grid.grid[rowIndex][colIndex] == -1) //the player has clicked on a mine
			{
				for(rowIndex = 0; rowIndex < height; rowIndex ++)
					for(colIndex = 0; colIndex < width; colIndex ++)
						grid.discoveredSquare[rowIndex][colIndex] = true; //reveal the entire grid

				update();
				game.gamePanel.setBackground(loseBackGroundColor); //change the color of the panel
				return;
			}

			if(grid.grid[rowIndex][colIndex] == 0) //0 has been clicked on, call the floodfill method
			{
				grid.floodFill(grid.grid, rowIndex, colIndex);
				update(); //change the look of all the buttons
			}
			else
				changeClickedButtonLook(game.buttons[rowIndex][colIndex], grid.grid[rowIndex][colIndex]); //change the look of the button that's been clicked on

			if(grid.won()) //the player has won the game
			{
				game.gamePanel.setBackground(winBackGroundColor); //change the color of the panel
				for(int row = 0; row < height; row ++)
					for(int col = 0; col < width; col ++)
						if(!grid.discoveredSquare[row][col])
						{
							game.buttons[row][col].setEnabled(false); //set all the mines as unclickable
							game.buttons[row][col].setIcon(new ImageIcon("res/icons/-1.png"));
						}
			}
		}
	}

	public class RightClick extends MouseAdapter implements MouseListener
	{
		private int rowIndex, colIndex;

		public RightClick(int rowIndex, int colIndex)
		{
			this.rowIndex = rowIndex;
			this.colIndex = colIndex;
		}

		public void mouseClicked(MouseEvent e)
		{
			if(e.getButton() == 3)
				game.buttons[rowIndex][colIndex].setIcon(new ImageIcon("res/icons/-1.png")); //set the icon of the clicked button to a mine
		}
	}
}